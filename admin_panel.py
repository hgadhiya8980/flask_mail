from datetime import datetime
import random

from flask import (flash, Flask, redirect, render_template, request,
                   session, url_for)
from pymongo import MongoClient
from flask_mail import Mail
import urllib.request
import os
from werkzeug.utils import secure_filename
from constant import all_constants

app = Flask(__name__)
UPLOAD_FOLDER = all_constants["upload_folder"]

app.config["DEBUG"] = all_constants["debug"]
app.config["SECRET_KEY"] = all_constants["secret_key"]

# configuration of mail
app.config['MAIL_SERVER'] = all_constants["mail_config"]["mail_server"]
app.config['MAIL_PORT'] = all_constants["mail_config"]["mail_port"]
app.config['MAIL_USERNAME'] = all_constants["mail_config"]["mail_username"]
app.config['MAIL_PASSWORD'] = all_constants["mail_config"]["mail_password"]
app.config['MAIL_USE_SSL'] = all_constants["mail_config"]["mail_use_ssl"]
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

mail = Mail(app)

secure_type = all_constants["secure_type"]
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif' 'svg'])
ALLOWED_EXTENSIONS_file = set(['pdf', 'doc'])

# allow photo extension
def allowed_photos(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

#allow file extension
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_file

# link validation
link_verificattion = ["https", "http", "www", "com", "org", "edu", "in"]

####database connectivity
client = MongoClient(all_constants["mongo_url"])

db = client[all_constants["database_name"]]

def register_data(coll_name, new_dict):
    try:
        coll = db[coll_name]
        coll.insert_one(new_dict)

        return "add_data"

    except Exception as e:
        print(e)

def find_all_cust_details():
    try:
        coll = db["customer_details"]
        res = coll.find({})
        return res

    except Exception as e:
        print(e)

def find_all_cust_details_coll(coll_name):
    try:
        coll = db[coll_name]
        res = coll.find({})
        return res

    except Exception as e:
        print(e)

def find_all_specific_user(coll_name, di):
    try:
        coll = db[coll_name]
        res = coll.find(di)
        return res

    except Exception as e:
        print(e)

def delete_data(coll_name, di):
    try:
        coll = db[coll_name]
        res = coll.delete_one(di)
        return res

    except Exception as e:
        print(e)

def update_data(coll_name, prev_data, update_data):
    try:
        coll = db[coll_name]
        coll.update_one(prev_data, update_data)
        return "updated"

    except Exception as e:
        print(e)

def checking_upload_folder(filename):
    try:
        entries = os.listdir(app.config["UPLOAD_FOLDER"])
        if filename in entries:
            return "duplicate"
        else:
            return "not duplicate"

    except Exception as e:
        print(e)

# That function should be login into that product
@app.route("/", methods=["GET", "POST"])
def admin_panel():
    """
    That route can use login user
    """
    try:
        if request.method == "POST":
            username = request.form["username"]
            pwd = request.form["password"]

            res = find_all_cust_details_coll(coll_name="admin_user")
            username_list = [[data.get("username", ""), data.get("password", ""), data.get("email", "")] for data in res]

            for var in username_list:
                if [username, pwd] in var[0:2]:
                    session["email"] = username_list[2]
                    session["username"] = username
                    flash("Successfully Login")
                    return redirect(url_for('otp_sending', _external=True, _scheme=secure_type))
            flash("Your credentials doesn't match! Please enter correct Username and password...")
            return render_template("login.html")
        else:
            return render_template("login.html")

    except Exception as e:
        flash("Please try again..............................")
        return render_template("login.html")


# That is route for otp sending mail for user
@app.route("/otp_sending", methods=["GET","POST"])
def otp_sending():
    """
    That funcation was sending a otp for user
    """

    try:
        otp = random.randint(100000, 999999)
        session["otp"] = otp
        mail.send_message("OTP Received",
                          sender="codescatter8980@gmail.com",
                          recipients=[session.get("email", "")],
                          body='Hello {0}\nYour OTP is {1}\nThis OTP is valid only 10 minutes....'.format(session["username"], otp))

        return redirect(url_for('forget_password', _external=True, _scheme=secure_type))

    except Exception as e:
        flash("Please try again.......................................")
        return redirect(url_for('forget_password', _external=True, _scheme=secure_type))


@app.route("/home", methods=["GET", "POST"])
def home():
    """
    That route can use login user
    """
    try:
        return render_template("index.html")

    except Exception as e:
        flash("Please try again..............................")
        return render_template("index.html")



if __name__ == "__main__":
    # db.create_all()
    app.run(
        host="127.0.0.1",
        port="8100",
        debug=True)

